#    dircolors.legacy
#    Copyright (C) 1996-2018 Free Software Foundation, Inc.
#    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This file is part of ls-libre.
#
#    ls-libre is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ls-libre is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ls-libre.  If not, see <https://www.gnu.org/licenses/>.
#
#    Configuration file for dircolors, a utility to help you set the
#    LS_COLORS environment variable used by GNU ls with the --color option.
#
#    Copying and distribution of this file, with or without modification,
#    are permitted provided the copyright notice and this notice are preserved.
#    The keywords COLOR, OPTIONS, and EIGHTBIT (honored by the
#    slackware version of dircolors) are recognized but ignored.
#    Below are TERM entries, which can be a glob patterns, to match
#    against the TERM environment variable to determine if it is colorizable.

TERM Eterm
TERM ansi
TERM color-xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM console
TERM cygwin
TERM dtterm
TERM gnome
TERM konsole
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM mlterm
TERM putty
TERM rxvt
TERM rxvt-cygwin
TERM rxvt-cygwin-native
TERM rxvt-unicode
TERM screen
TERM screen-bce
TERM screen-w
TERM screen.linux
TERM vt100
TERM xterm-256color
TERM xterm-color
TERM xterm-debian

# normal file
#
# use defaults
NORMAL                0
FILE                  0

# directory
#
# Purple3
DIR                   38;5;56

# symbolic link
#
# Grey15
LINK                  38;5;235

# broken symbolic link
#
# Grey15 reversed
ORPHAN                38;5;250;48;5;235

# target of broken symbolic link
# pipe
# socket
# door
# block device driver
# character device driver
#
# treat as normal files
MISSING               0
FIFO                  0
SOCK                  0
DOOR                  0
BLK                   0
CHR                   0

# other shit
#
# treat as normal files
SETUID                0
SETGID                0
STICKY_OTHER_WRITABLE 0
OTHER_WRITABLE        0
STICKY                0

# executables
#
# bold
# Red1
EXEC                  1;38;5;196

# libra
#
# bold
# GreenYellow
.epub  1;38;5;154
.pdf   1;38;5;154
.fb2   1;38;5;154
.azw   1;38;5;154 # ebaniy styd
.azw3  1;38;5;154 # ebaniy styd
.kfx   1;38;5;154 # ebaniy styd
.mobi  1;38;5;154 # ebaniy styd
.lit   1;38;5;154 # ebaniy styd
.lrf   1;38;5;154 # ebaniy styd
.djvu  1;38;5;154
.html  1;38;5;154
.htm   1;38;5;154
.css   1;38;5;154
.doc   1;38;5;154 # ebaniy styd
.docx  1;38;5;154 # ebaniy styd
.rtf   1;38;5;154
.txt   1;38;5;154
.odf   1;38;5;154

# codibra
#
# DarkGreen
.c       38;5;22
.h       38;5;22
.pl      38;5;22
.pm      38;5;22
.cpp     38;5;22    # ebaniy styd
.cc      38;5;22    # ebaniy styd
.hh      38;5;22    # ebaniy styd
.vim     38;5;22
.py      38;5;22
.vala    38;5;22
.vapi    38;5;22
.p6      38;5;22
.raku    38;5;22
.rakumod 38;5;22

# audio formats
#
# bold
# DeepPink1
.aac   1;38;5;199 # ebaniy styd
.au    1;38;5;199
.flac  1;38;5;199
.m4a   1;38;5;199
.mid   1;38;5;199
.midi  1;38;5;199
.mka   1;38;5;199
.mp3   1;38;5;199 # ebaniy styd
.mpc   1;38;5;199
.ogg   1;38;5;199
.ra    1;38;5;199
.wav   1;38;5;199
.oga   1;38;5;199
.opus  1;38;5;199
.spx   1;38;5;199
.xspf  1;38;5;199
.xm    1;38;5;199
.mod   1;38;5;199
.s3m   1;38;5;199
.it    1;38;5;199
.sc68  1;38;5;199
.wma   1;38;5;199
.sap   1;38;5;199
.sid   1;38;5;199
.rad   1;38;5;199
.d00   1;38;5;199
.v2m   1;38;5;199
.ahx   1;38;5;199
.ym    1;38;5;199
.fc14  1;38;5;199
.fc15  1;38;5;199

# image & video formats
#
# Magenta3
.jpg   38;5;163
.jpeg  38;5;163
.mjpg  38;5;163
.mjpeg 38;5;163
.gif   38;5;163
.bmp   38;5;163
.pbm   38;5;163
.pgm   38;5;163
.ppm   38;5;163
.tga   38;5;163
.xbm   38;5;163
.xpm   38;5;163
.tif   38;5;163
.tiff  38;5;163
.png   38;5;163
.svg   38;5;163
.svgz  38;5;163
.mng   38;5;163
.pcx   38;5;163
.mov   38;5;163
.mpg   38;5;163
.mpeg  38;5;163
.m2v   38;5;163
.mkv   38;5;163
.webm  38;5;163
.ogm   38;5;163
.mp4   38;5;163
.m4v   38;5;163
.mp4v  38;5;163
.vob   38;5;163
.qt    38;5;163
.nuv   38;5;163
.wmv   38;5;163
.asf   38;5;163
.rm    38;5;163
.rmvb  38;5;163
.flc   38;5;163
.avi   38;5;163
.fli   38;5;163
.flv   38;5;163
.gl    38;5;163
.dl    38;5;163
.xcf   38;5;163
.xwd   38;5;163
.yuv   38;5;163
.cgm   38;5;163
.emf   38;5;163
.ogv   38;5;163
.ogx   38;5;163
.nef   38;5;163

# archives or compressed
#
# Grey35
.tar   38;5;240
.tgz   38;5;240
.arc   38;5;240
.arj   38;5;240
.taz   38;5;240
.lha   38;5;240
.lz4   38;5;240
.lzh   38;5;240
.lzma  38;5;240
.tlz   38;5;240
.txz   38;5;240
.tzo   38;5;240
.t7z   38;5;240
.zip   38;5;240
.z     38;5;240
.dz    38;5;240
.gz    38;5;240
.lrz   38;5;240
.lz    38;5;240
.lzo   38;5;240
.xz    38;5;240
.zst   38;5;240
.tzst  38;5;240
.bz2   38;5;240
.bz    38;5;240
.tbz   38;5;240
.tbz2  38;5;240
.tz    38;5;240
.deb   38;5;240
.rpm   38;5;240
.jar   38;5;240
.war   38;5;240
.ear   38;5;240
.sar   38;5;240
.rar   38;5;240
.alz   38;5;240
.ace   38;5;240
.zoo   38;5;240
.cpio  38;5;240
.7z    38;5;240
.rz    38;5;240
.cab   38;5;240
.wim   38;5;240
.swm   38;5;240
.dwm   38;5;240
.esd   38;5;240
.img   38;5;240
.iso   38;5;240
.fs    38;5;240
